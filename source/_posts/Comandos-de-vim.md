---
title: Comandos de vim
date: 2019-04-14 22:26:36
tags:
---

#### Movimientos simples del cursor
```
h = Mover el cursor a la izquierda
j = Mover el cursor abajo
k = Mover el cursor arriba
l = Mover el cursor a la derecha
```

#### Scroll


###### Movimiento de la ventana
```
CTRL + E = Mover scroll al final de la ventana
CTRL + Y = Mover scroll al principio de la ventana
CTRL + F = Mover scroll una pagina abajo
CTRL + B = Mover scroll una pagina arriba

```
###### Movimiento de cursor
```
H = Mover el cursor a la parte superior de la ventana
M = Mover el cursor a la parte media de la ventana
L = Mover el cursor a la parte baja de la ventana
gg = Mover el cursor al principio de la pagina
```
## Visual mode

#### Selección de texto

```

aw = Seleccionar una palabra incluyendo espacios
iw = Seleccionar una palabra sin incluir espacios
aW = Seleccionar una palabra incluyendo espacios
iW = Seleccionar una palabra sin incluir espacios 
as = Selecciona una sentencia con espacios
is = Selecciona una sentencia sin espacios
ap = Selecciona un parrafo con espacios
ip = Selecciona un parrafo sin espacios

```


## Plugins

#### NERDTree

```
CTRL + w + w = moverse entre la ventana abierta y el nerdtree
```
###### Split (Dividir la pantalla)

```
i = Dividir la ventana (la nueva ventana aparecera abajo)
gi = Dividir la ventana (la nueva ventana aparecera arriba)
s = Dividir la pantalla en vertical (la nueva ventana aparecera a la derecha)
gs = Dividir la pantalla en vertical (la nueva ventana aparecera a la izquierda)
CTRL + W + = Aumentar ventana de forma horizontal
CTRL + W - = Disminuir la ventana de forma horizontal
CTRL + W < = Disminuir la ventana de forma vertical
CTRL + W > = Aumentar la ventana de forma vertical

``` 

###### Pestañas (Tabs)
```
gt = Siguiente pestaña
gT = Anterior pestaña
```
