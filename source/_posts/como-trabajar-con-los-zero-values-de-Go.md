---
title: Como trabajar con los zero values de Go 
date: 2020-06-18 11:05
tags: Go
---

En Go tras declarar una variable no es necesario inicializarla si así lo creemos prudente, o si simplemente olvidamos hacerlo. automaticamente se asigna un valor por defecto a las variables de acuerdo a su tipo de dato:


| Zero vaue | Tipo de dato                                             |
|-----------|----------------------------------------------------------|
| 0         | Integer                                                  |
| 0.0       | Floating pointer                                         |
| ""        | String                                                   |
| false     | Boolean                                                  |
| nil       | Interfaces, slices, channels, maps, pointers y funciones |
Tabla inspirada en https://yourbasic.org/golang/default-zero-value/


Ejemplo de funcionamiento en Go:

```Go

//Creamos las variable sin asignar valor alguno.
var prueba int
var prueba2 bool

//Esto nos imprimira un 0
fmt.Println(prueba)

//Esto nos imprimira un false
fmt.Println(prueba2)

```

Esto es de utilidad debido a que podemos estar seguros de que al llamar a una variable siempre tendrá un valor valido, eliminando la necesidad de verificar si nuestra variable fue inicializada con anterioridad. Pero todo poder tiene una consecuencia, y aquí esta un comportamiento interesante al trabajar con estructuras JSON (Omito de forma deliberada los tags correspondientes a JSON en la estructura):


Supongamos que tenemos el siguiente código:

 ```Go
 
 type Paquete struct{ 

   //Estado representa el estado actual del proceso de envio de un paquete
   //0 - Pendiente de envio
   //1 - Enviado
   //2 - Entregado
   //3 - Problemas en su entrega
   Estado          int

   //Notas representa un campo para colocar datos adicionales del paquete
   Notas           string

   //Numero de rastreo
   NumeroDeRastreo string
 }

 ```
 
 Nuestro cliente envía en forma de un JSON los datos para indicar que hay un problema con un paquete de un comprador, y crea una estructura con la siguiente información:

```JSON
{
   "notas"           : "El paquete presenta un problema, se requiere contactar al cliente",
   "NumeroDeRastreo" : 1234ABS1234
}

``` 


Como notaremos, nuestro cliente olvido colocar cual es el estado del paquete, por lo que Go al deserializar los datos nos crea una estructura como la siguiente:


 ```Go
 
 paquete := &Paquete{ 
   Estado: 0,
   Notas:  "El paquete presenta un problema, se requiere contactar al cliente",
   NumeroDeRastreo: 1234ABS1234,
 }

if estadoValido(paquete.Estado) {
   //Todo esta bien por aqui
}

 ```
 
Go hizo el llenado de nuestra estructura en base al JSON de forma correcta, y nos coloca el zero value correspondiente para los campos que se omitieron por parte del JSON, evitando que el sistema falle en tiempo de ejecución. Esto por así llamarlo es un bug silencioso, y lo mas probable es que llegue a producción sin ser corregido, después de todo las pruebas han pasado de forma positiva y el editor de código no te a advertido sobre algún problema.

Ciertamente esta característica puede ser peligrosa si lo vemos de esta forma, pero existe una solución para esto

## Utilizar punteros como solución.

Si observamos al principio de esta entrada coloque una tabla con los zero values, entre ellos encontramos que para ciertos tipos, el nil es su valor por defecto y el utilizarlos nos brindaran una programación mas similar a la encontrada en otros lenguajes de programación que no tienen zero values.


 ```Go
 
 type Paquete struct{ 

   //Estado representa el estado actual del proceso de envio de un paquete
   //0 - Pendiente de envio
   //1 - Enviado
   //2 - Entregado
   //3 - Problemas en su entrega
   Estado          *int

   //Notas representa un campo para colocar datos adicionales del paquete
   Notas           *string

   //Numero de rastreo
   NumeroDeRastreo *string
 }

 ```
Como resultado al recibir el JSON planteado anteriormente nuestra estructura llena se vera asigna

 ```Go
 
 paquete := &Paquete{ 
   Estado: <nil>,
   Notas:  "El paquete presenta un problema, se requiere contactar al cliente",
   NumeroDeRastreo: 1234ABS1234,
 }

//Ahora nuestro validador podra detectar si el valor del estado es legitimo
if estadoValido(paquete.Estado) {
   //Todo esta bien por aqui
}

 ```
