---
title: GoMocks 
date: 2020-06-19 19:13:00
tags:
---


## ¿Que son los mocks?

Los mocks son objetos que podemos utilizar en nuestro código de prueba para emular los objetos complejos que nos costaría trabajo instanciar para utilizarlo. Por ejemplo al probar un servicio es probable que necesitemos llamar a nuestro repositorio, pero el hacerlo implica hacer movimientos que no competen a una prueba orientada un servicio, preparar una base de datos con el comportamiento requerido brinda mayor complejidad a nuestra prueba. Es en este punto donde entran los mocks, que emulan ser nuestro repositorio, permitiendo ejecutar nuestra prueba de servicio sin preocuparnos por que hace o deja de hacer el repositorio falso que nuestro mock esta intentando ser.

## GoMock

El repositorio de Golang cuenta con un proyecto que nos brinda la generacion automatica de mocks, basados en la interfase que le indiquemos
